# -*- coding UTF-8 -*-
require 'rdbi'
require 'rdbi-driver-sqlite3'
require 'date'
require './HouseholdBookInfo'

class HouseholdBookManager
  # 初期化
  def initialize ( userinfos )
    # データベースに接続
    $db_name = userinfos
    $dbh = RDBI.connect( :SQLite3, :database => $db_name )
  end

  def addHouseBookInfo
    puts "\n1:データの登録"
    print "\n商品データを登録します。"

    household_book_info = HouseholdBookInfo.new( "", 0, Date.new)
    print "\n"
    print "ID(数字を入力してください。)："
    id = gets.chomp
    print "商品名: "
    household_book_info.product_name = gets.chomp
    print "金額: "
    household_book_info.sum = gets.chomp.to_i
    puts "購入日"
    print "年："
    year = gets.chomp.to_i
    print "月："
    month = gets.chomp.to_i
    print  "日："
    day = gets.chomp.to_i
    household_book_info.date = Date.new( year, month, day )

    # データベースの値を配列に入れる
    sth_id = $dbh.execute("select id from household_account_books;").fetch(:all)
    # 二次元配列を一次元配列にして取得したIDとデータベースのIDを比較
    # IDが存在しなければデータを登録、存在すればaddHouseBookInfoに戻る
    if sth_id.flatten!.include?(id.to_i)
      puts "\nこのIDは既に存在するので、異なるIDを指定してください。"
      addHouseBookInfo 
    else
      $dbh.execute("insert into household_account_books values( ?, ?, ?, ?);",
        id,
        household_book_info.product_name,
        household_book_info.sum,
        household_book_info.date.to_s )
      puts "\n登録しました。"
    end
  end

  def listAllHouseBookInfos
    item_name = { 'id' => "ID" , 'product_name' => "商品名", 'sum' => "金額",  'date' => "日付" }

    puts "\n2:データの一覧表示"
    puts "\nデータを表示します。\n"

    # テーブルからデータを読み込んで表示する
    sth = $dbh.execute("select * from household_account_books;")
 
        # 表示の件数をカウント
    counts = 0
    sth.each do |row|
      puts "--------------------------------"
      # each_with_nameメソッドで値と項目名を取り出して表示
      row.each_with_index do |val, name| 
        puts "#{item_name.values[name]} :  #{val.to_s}"
      end
      puts "--------------------------------"
      counts = counts + 1
    end
    # 実行結果を解放する
    sth.finish

    puts "\n#{counts}件表示しました。"
  end

  def deleteHouseBookInfo
    puts "\n3:データの削除"
    print "\nIDを入力してください。"
    id = gets.chomp
    
    item_name = { 'id' => "ID" , 'product_name' => "商品名", 'sum' => "金額",  'date' => "日付" } 

    # 取得したIDのデータを取得
    sth = $dbh.execute ("select * from household_account_books where id = #{id};")
    # データベースの値を配列に入れる
    sth_id = $dbh.execute("select id from household_account_books;").fetch(:all)
    # 二次元配列を一次元配列にして取得したIDとデータベースのIDを比較
    # IDが存在すればデータを表示、存在しなければdeleteHouseBookInfoに戻る
    if sth_id.flatten!.include?(id.to_i)

      sth.each do |row|
        puts "--------------------------------"
        # each_with_indexメソッドで値と項目名を取り出して表示
        row.each_with_index do |val, name|
          puts "#{item_name.values[name]} :  #{val.to_s}"
        end
        puts "--------------------------------"
      end
      sth.finish
    else
      puts "\nこのIDはありません。"
      deleteHouseBookInfo
    end

    print "このデータを削除しますか？（削除する場合はY/yを入力してください。）\n"
    yesno = gets.chomp.upcase
    if /^Y$/ =~ yesno
      sth = $dbh.execute ( "delete from household_account_books where id = #{id} ;" ) 
      sth.finish
      print "\n削除しました。\n\n"
    else  
      deleteHouseBookInfo
    end
 
  end

  def aggregationGameInfo
    puts "\n4:集計データの表示"
  
    item_name = { 'user_id' => "ユーザーID" , 'user_name' => "ユーザーネーム" , 'gender' => "性別" , 'money' => "所持金" , 'item' => "アイテム数" , 'total_money' => "合計金額" }

    sth = $dbh.execute("select * from userinfo;")

    sth.each do |row|
      puts "----------------------------------------------"
      # each_with_indexメソッドで値と項目名を取り出して表示
      row.each_with_index do |val, name|
        puts "#{item_name.values[name]} :  #{val.to_s}"
      end
      puts "----------------------------------------------"
    end 
  end

  def statisticsGameInfo
    puts "\n5:統計データの表示"
    
    item_name_1 = { 'user_id' => "ユーザーID", 'user_name' => "ユーザーネーム" , 'mode' => "サイコロの目の最頻値" , 'sum' => "サイコロの出た目の和" , 'number_of_times' => "サイコロを振った回数"}

    sth = $dbh.execute("select * from statistics;")
   
    sth.each do |row|
      puts "-----------------------------------------------"
      # each_with_indexメソッドで値と項目名を取り出して表示
      row.each_with_index do |val, name|
        puts "#{item_name_1.values[name]} :  #{val.to_s}"
      end
      puts "-----------------------------------------------"
    end  
  
    item_name_2 = {"user_name" => "ユーザーネーム" , "total_money" => "合計金額"}

    sth = $dbh.execute("select user_name,total_money from userinfo;")
    array = []

    puts "\nゲームの結果よりアドバイスがあります。"
    puts "\nY/yを押してください。"
 
    yesno = gets.chomp.upcase
    if /^Y$/ =~ yesno
      sth.each do |row|
        row.each_with_index do |val,name|
          array.push(val)
        end
        $hash = Hash[*array]
      end

      $hash.each do |key, value|
      puts "-----------------------------------------------"
        puts "#{item_name_2["user_name"]} :  #{key}"
        puts "#{item_name_2["total_money"]} : #{value}"
        if value > 0
          puts "\nこの調子で次も頑張って！"
        else
          puts "\nもう少し頑張ろう。"
        end
      puts "-----------------------------------------------"
       
      end
    else
      statisticsGameInfo
    end

  end

  def run
    while true
      print"
～家計簿～
1:データの登録
2:データの一覧表示
3:データの削除

～ゲームのデータ～
4:集計データの表示
5:統計データの表示

9:アプリの終了
1～5,9の番号を選んでください。"

      num = gets.chomp
      case num
      when '1' 
        addHouseBookInfo
      when '2' 
        listAllHouseBookInfos
      when '3' 
        deleteHouseBookInfo
      when '4' 
        aggregationGameInfo
      when '5' 
        statisticsGameInfo
      when '9' 
        $dbh.disconnect
        puts "終了しました。\n"
        break;
      else

      end
    end
  end

end

household_book_manager = HouseholdBookManager.new("../userinfos.db")

household_book_manager.run

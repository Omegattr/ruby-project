# -*- coding:utf-8 -*-
require 'date'

class HouseholdBookInfo
  # インスタンスの初期化
  def initialize ( product_name, sum, date )
    @product_name = product_name
    @sum = sum
    @date = date
  end

  #　アクセサ提供
  attr_accessor  :product_name, :sum, :date

  # インスタンスの文字列表現を返す
  def to_s
    " #{@product_name}, #{@sum}, #{@date}"
  end

  # フォーマット
  def toFormattedString ( sep = "\n" )
    " 商品名：#{@product_name}#{sep} 金額：#{@sum}#{sep} 日付：#{@date}#{sep}"
  end
end

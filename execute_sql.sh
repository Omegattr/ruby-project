#!/bin/sh
sqlite3 userinfos.db < sql/dcl.sql
sqlite3 userinfos.db < sql/ddl_household_account_books.sql
sqlite3 userinfos.db < sql/ddl_statistics.sql
sqlite3 userinfos.db < sql/ddl_userinfo.sql
sqlite3 userinfos.db < sql/dml_household_account_books.sql
sqlite3 userinfos.db < sql/dml_statistics.sql
sqlite3 userinfos.db < sql/dml_userinfo.sql


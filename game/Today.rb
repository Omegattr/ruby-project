#require 'rdbi'
#require 'rdbi-driver-sqlite3'
require './Present'
require './Cretaceous'
require './Nara'
#require 'color_echo'

class Today
  #アイテムの個数を集計し、金額に換算するメソッド
  def count
    CE.pickup(/\d/, :index227, backgruond=nil, :bold )
    present_conversion = Present.new
    cretaceous_conversion = Cretaceous.new
    nara_conversion = Nara.new
    $sword_flag = 0 if $sword_lose_flag == 0
    if $sword_flag == 1
      sword_conversion = 100000
    elsif $sword_flag == 0
      sword_conversion = 0
    end
    puts "\n\n\n\n\n\n"
    puts "           --------取得したアイテム---------------------"
    puts "\n                刀：#{$sword_flag}本 -------> #{sword_conversion}ruby"
    corner_conversion = $corner_flag * 100000
    treasure_conversion = $treasure_flag * 200000
    japan_coin_conversion = $japan_coin_flag * 150000
    puts "\n                角：#{$corner_flag}個獲得 -------> #{corner_conversion}ruby"
    puts "\n                村の宝：#{$treasure_flag}個獲得 -------> #{treasure_conversion}ruby"
    puts "\n               和同開珎銀銭：#{$japan_coin_flag}個獲得 -------> #{japan_coin_conversion}ruby"
    #sth = $dbh.execute("select user_name,money from userinfo where user_id = #{$data_number};")
    #sth.each do |row|
    #  printf("\nユーザネーム: %s, 所持金: %s", row[0], row[1])
    #end
    #print " --------> "
    sth = $dbh.execute("update userinfo set money = money + #{sword_conversion} where user_id = #{$data_number};")
    sth = $dbh.execute("update userinfo set money = money + #{corner_conversion} where user_id = #{$data_number};")
    sth = $dbh.execute("update userinfo set money = money + #{treasure_conversion} where user_id = #{$data_number};")
    sth = $dbh.execute("update userinfo set money = money + #{japan_coin_conversion} where user_id = #{$data_number};")
    sth = $dbh.execute("select money from userinfo where user_id = #{$data_number};")
    sth.each do |row|
    #  printf("所持金: %s", row[0])
    #  puts ""
      puts "\n\n\n\n"
      @money = row[0]
    end
    $dbh.execute("update userinfo set total_money = '#{@money}' where user_id = #{$data_number};")
    puts "\n(Enterを押してください。)"
    gets.chomp
    CE.reset(scope=:pickup)
  end

  #サイコロの目の統計データを表示するメソッド
  def dice_count
    CE.pickup(/\d/, :index227, backgruond=nil, :bold )
    puts "\n\n\n\n\n\n          -------サイコロの出た目の回数-------\n"
    puts "                 １の目: #{$throw_one_count}回"
    puts "                 ２の目: #{$throw_two_count}回"
    puts "                 ３の目: #{$throw_three_count}回"
    puts "                 ４の目: #{$throw_four_count}回"
    puts "                 ５の目: #{$throw_five_count}回"
    puts "                 ６の目: #{$throw_six_count}回"
    puts ""
    sum = $throw_one_count + $throw_two_count + $throw_three_count + $throw_four_count + $throw_five_count + $throw_six_count
    puts "\n              合計：#{sum}回"
    puts "\n\n\n"
    puts"(Enterを押してください。)"
    gets.chomp
    puts "\n\n\n\n\n\n          -------サイコロの出た目の確率--------"
    dice_one = $throw_one_count * 100 / sum
    dice_two = $throw_two_count * 100 / sum
    dice_three = $throw_three_count * 100 / sum
    dice_four = $throw_four_count * 100 / sum
    dice_five = $throw_five_count * 100 / sum
    dice_six = $throw_six_count * 100 / sum
    puts "                 １の目: #{dice_one}%"
    puts "                 ２の目: #{dice_two}%"
    puts "                 ３の目: #{dice_three}%"
    puts "                 ４の目: #{dice_four}%"
    puts "                 ５の目: #{dice_five}%"
    puts "                 ６の目: #{dice_six}%"
    puts ""
    puts ""

    array = [$throw_one_count,$throw_two_count,$throw_three_count,$throw_four_count,$throw_five_count,$throw_six_count]
    mode = array.max
    if mode == $throw_one_count
      mode = 1
    elsif mode == $throw_two_count
      mode = 2
    elsif mode == $throw_three_count
      mode = 3
    elsif mode == $throw_four_count
      mode = 4
    elsif mode == $throw_five_count
      mode = 5
    elsif mode == $throw_six_count
      mode = 6
    end
    sth = $dbh.execute("update statistics set mode = #{mode} where user_id = #{$data_number};")
    sth = $dbh.execute("update statistics set sum = #{sum} where user_id = #{$data_number};")
    sth = $dbh.execute("update statistics set number_of_times = #{sum} where user_id = #{$data_number};")
    sth = $dbh.execute("select user_name,mode,sum from statistics where user_id = #{$data_number};")
    sth.each do |row|
      printf("\n             ユーザネーム: %s, 最頻値: %sの目, 合計回数: %s回", row[0], row[1], row[2])
      puts ""
      puts ""
    end
    puts "\n(Enterを押してください。)"
    gets.chomp
    CE.reset(scope=:pickup)
  end

  def ending
    CE.pickup(/\d/, :index227, backgruond=nil, :bold )
    CE.pickup("ゲームオーバー", :red, backgruond=nil, :bold )
    CE.pickup("ゲームクリア", :blue, backgruond=nil, :bold )
    thread = Thread.new do
      sleep (0.1)
      while true
        str = "成績"
        str = ('-' * 50) + str
        str.size.times do |i|
          print str
          sleep 0.08
          print "\r"
          str.slice!(0)

          # 末尾に半角スペースを追加
          str += '-'
        end
      end
    end
    puts "\n\n\n\n\n\n"
    
    puts "                       ＼ 成績発表 ／                     "
    puts "\n\n\n"
    puts "            ユーザネーム： #{$user_name}"
    puts ""
    item = $sword_flag + $corner_flag + $treasure_flag + $japan_coin_flag
    puts "            アイテム：#{item}個"
    puts ""
    sth = $dbh.execute("select money from userinfo where user_id = #{$data_number};")
    sth.each do |row|
      @money = row[0]
    end
    puts "            所持金：#{@money}"
    
    if @money >= 0
      puts "\n            ゲームクリア"
    else
      puts "\n            ゲームオーバー"
    end
    puts "\n\n\n"
    flag = gets.chomp.to_i
    if flag == 1 then
      Thread::kill(thread)
    else
      Thread::kill(thread)
    end
    CE.reset(scope=:pickup)
  end

  #最初の画面に戻るメソッド
  def run
    while true
      print "
はじめに戻ります
「1」を入力してください"

      num = gets.chomp.to_i
      if num == 1
        $throw_one_count = 0
        $throw_two_count = 0
        $throw_three_count = 0
        $throw_four_count = 0
        $throw_five_count = 0
        $throw_six_count = 0
        require './Edo'
        edo_entry = EdoEntry.new("../userinfos.db")
        edo_entry.run
      end
    end
  end
end

#ゲームの終了条件を記載したクラス
class EndCondition
  def finish
    count = $throw_one_count + $throw_two_count + $throw_three_count + $throw_four_count + $throw_five_count + $throw_six_count
    if 30 < count
      puts "30回以上サイコロを振ってしまった。ゲーム終了"
      require './Today'
      conversion = Today.new
      conversion.run
    end
  end
end
#require 'color_echo' #文字色、背景色を色付けする
#require 'rdbi'
#require 'rdbi-driver-sqlite3'
#require './AAlist'

class Italy

  def branch
    italy_logo = AAlist.new
    italy_logo.italy
    puts "(Enterを押してください。)"
    gets.chomp
    
    #サイコロの目の回数を数えるための変数
    one_event = 0
    two_event = 0
    three_event = 0
    four_event = 0
    five_event = 0
    loop do
      puts "\nサイコロを振ります。(Y/yを押してください。)"
      yesno = gets.chomp.upcase
      print "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n"
      if /^Y$/ =~ yesno
        number = rand(6) + 1
        one_event += 1 if number == 1
        two_event += 1 if number == 2
        three_event += 1 if number == 3
        four_event += 1 if number == 4
        five_event += 1 if number == 5
        if one_event >= 3 || two_event >= 3 || three_event >= 3 || four_event >= 3 || five_event >= 3 then
          number = 6
          # サイコロの目を表示
          dice = AAlist.new
          case number
          when 1
            dice.dice1
          when 2
            dice.dice2
          when 3
            dice.dice3
          when 4
            dice.dice4
          when 5
            dice.dice5
          when 6
            dice.dice6
          end
          puts "(Enterを押してください。)"
          gets.chomp

        else
          # サイコロの目を表示
          dice = AAlist.new
          case number
          when 1
            dice.dice1
          when 2
            dice.dice2
          when 3
            dice.dice3
          when 4
            dice.dice4
          when 5
            dice.dice5
          when 6
            dice.dice6
          end
          puts "(Enterを押してください。)"
          gets.chomp

        end
      end

      case number
      when 1
        $throw_one_count += 1
        puts "\nマルコポーロを誘い、アイスを食べる。"
        puts "マルコポーロが仲間を呼んだのでみんなのアイスもご馳走することになった。。。\n"
        puts "\nー豆知識ー
アイスクリームがシルクロードを通り、中国からイタリアに伝わったという説があります。
それを持ち帰ったのがマルコポーロだと言われています。"
        puts "1万ruby使用した。"
        sth = $dbh.execute("select user_name,money from userinfo where user_id = #{$data_number};")
        sth.each do |row|
          printf("ユーザネーム: %s, 所持金: %s", row[0], row[1])
        end

        print " --------> "
        sth = $dbh.execute("update userinfo set money = money - 10000 where user_id = #{$data_number};")
        sth = $dbh.execute("select money from userinfo where user_id = #{$data_number};")
        sth.each do |row|
          printf("所持金: %s", row[0])
          puts ""
          puts ""
        end
        puts "\n(Enterを押してください。)"
        gets.chomp
        print "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n"

      when 2
        $throw_two_count += 1
        puts "\n貴族に気に入られ酒池肉林な日々を過ごした。さらに財宝まで頂く。500万rubyを得る。"
        sth = $dbh.execute("select user_name,money from userinfo where user_id = #{$data_number};")
        sth.each do |row|
          printf("ユーザネーム: %s, 所持金: %s", row[0], row[1])
        end

        print " --------> "
        sth = $dbh.execute("update userinfo set money = money + 5000000 where user_id = #{$data_number};")
        sth = $dbh.execute("select money from userinfo where user_id = #{$data_number};")
        sth.each do |row|
          printf("所持金: %s", row[0])
          puts ""
          puts ""
        end
        puts "\n(Enterを押してください。)"
        gets.chomp
        print "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n"

      when 3
        $throw_three_count += 1
        puts "\nサイコロの力で国王になる。「#{$user_name}」王政の幕があけた。国有財産を全て自分のものにする。
9999万9999rubyを得る。"
        sth = $dbh.execute("select user_name,money from userinfo where user_id = #{$data_number};")
        sth.each do |row|
          printf("ユーザネーム: %s, 所持金: %s", row[0], row[1])
        end

        print " --------> "
        sth = $dbh.execute("update userinfo set money = money + 99999999 where user_id = #{$data_number};")
        sth = $dbh.execute("select money from userinfo where user_id = #{$data_number};")
        sth.each do |row|
          printf("所持金: %s", row[0])
          puts ""
          puts ""
        end
        puts "\n(Enterを押してください。)"
        gets.chomp
        print "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n"

      when 4
        $throw_four_count += 1
        puts "\n大きな戦いに参加して敗れる。1ruby以外の所持金を全て没収された。"
        sth = $dbh.execute("select user_name,money from userinfo where user_id = #{$data_number};")
        sth.each do |row|
          printf("ユーザネーム: %s, 所持金: %s", row[0], row[1])
          @money = row[1]
        end

        print " --------> "
        if @money > 0
          sth = $dbh.execute("update userinfo set money = money - money + 1 where user_id = #{$data_number};")

          sth = $dbh.execute("select money from userinfo where user_id = #{$data_number};")
          sth.each do |row|
            printf("所持金: %s", row[0])
            puts ""
            puts ""
          end

        else
          sth = $dbh.execute("select money from userinfo where user_id = #{$data_number};")
          sth.each do |row|
            printf("所持金: %s", row[0])
            puts ""
            puts ""
          end
        end
        puts "\n(Enterを押してください。)"
        gets.chomp
        print "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n"

      when 5
        $throw_five_count += 1
        puts "\n自宅に魔女が訪れた。「イヒヒヒィ。私と契約しないかい？」借金があれば０にしてくれる
契約だった。所持金が-（マイナス）なら0になる。"
        sth = $dbh.execute("select user_name,money from userinfo where user_id = #{$data_number};")
        sth.each do |row|
          printf("ユーザネーム: %s, 所持金: %s", row[0], row[1])
          @money = row[1]
        end

        print " --------> "
        if @money < 0
          sth = $dbh.execute("update userinfo set money = 0 where user_id = #{$data_number};")
          sth = $dbh.execute("select money from userinfo where user_id = #{$data_number};")
          sth.each do |row|
            printf("所持金: %s", row[0])
            puts ""
            puts ""
          end
        else
          sth = $dbh.execute("select money from userinfo where user_id = #{$data_number};")
          sth.each do |row|
            printf("所持金: %s", row[0])
            puts "\n借金が無かったので特に何も起こらなかった。"
            puts ""
            puts ""
          end
        end
        puts "\n(Enterを押してください。)"
        gets.chomp
        print "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n"

      when 6
        $throw_six_count += 1
        puts ""
        puts ""
        puts ""
        break

      end
    end
  end
end

#event = Italy.new
#event.branch

#require 'rdbi'
#require 'rdbi-driver-sqlite3'
#require 'color_echo'
#require './AAlist'

class Cretaceous
  #アイテム取得の個数を数える変数
  $corner_flag = 0
  $treasure_flag = 0

  def branch
    cretaceous_logo = AAlist.new
    cretaceous_logo.cretaceous
    puts "(Enterを押してください。)"
    gets.chomp

    #サイコロの目の回数を数えるための変数
    one_event = 0
    two_event = 0
    three_event = 0
    four_event = 0
    five_event = 0
    loop do
      puts "\nサイコロを振ります。(Y/yを押してください。)"
      yesno = gets.chomp.upcase
      print "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n"
      if /^Y$/ =~ yesno
        number = rand(6) + 1
        one_event += 1 if number == 1
        two_event += 1 if number == 2
        three_event += 1 if number == 3
        four_event += 1 if number == 4
        five_event += 1 if number == 5
        if one_event >= 3 || two_event >= 3 || three_event >= 3 || four_event >= 3 || five_event >= 3 then
          number = 6
          # サイコロの目を表示
          dice = AAlist.new
          case number
          when 1
            dice.dice1
          when 2
            dice.dice2
          when 3
            dice.dice3
          when 4
            dice.dice4
          when 5
            dice.dice5
          when 6
            dice.dice6
          end
          puts "(Enterを押してください。)"
          gets.chomp

        else
          # サイコロの目を表示
          dice = AAlist.new
          case number
          when 1
            dice.dice1
          when 2
            dice.dice2
          when 3
            dice.dice3
          when 4
            dice.dice4
          when 5
            dice.dice5
          when 6
            dice.dice6
          end
          puts "(Enterを押してください。)"
          gets.chomp

        end
      end

      case number
      when 1
        $throw_one_count += 1
        puts "\n恐竜に遭遇！！一緒にサイコロを振ることになった。"
        puts "\nサイコロを振ります。(Y/yを押してください。)"
        yesno = gets.chomp.upcase
        print "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n"
        if /^Y$/ =~ yesno
          number = rand(6) + 1
          dice = AAlist.new
          case number
          when 1
            dice.dice1
          when 2
            dice.dice2
          when 3
            dice.dice3
          when 4
            dice.dice4
          when 5
            dice.dice5
          when 6
            dice.dice6
          end
          puts "(Enterを押してください。)"
        end
        puts "\n次は恐竜が振ります。(Enterを押してください。)"
        gets.chomp
        print "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n"
        m_number = rand(6) + 1
        dice = AAlist.new
        case m_number
        when 1
          dice.dice1
        when 2
          dice.dice2
        when 3
          dice.dice3
        when 4
          dice.dice4
        when 5
          dice.dice5
        when 6
          dice.dice6
        end
        puts "(Enterを押してください。)"
        if number > m_number then
          puts"\n空から1万rubyが降ってきた。欠かさずゲット！！"
          sth = $dbh.execute("select user_name,money from userinfo where user_id = #{$data_number};")
          sth.each do |row|
            printf("\nユーザネーム: %s, 所持金: %s", row[0], row[1])
          end
          print " --------> "
          sth = $dbh.execute("update userinfo set money = money + 10000 where user_id = #{$data_number};")
          sth = $dbh.execute("select money from userinfo where user_id = #{$data_number};")
          sth.each do |row|
            printf("所持金: %s", row[0])
            puts ""
            puts ""
          end
        elsif number < m_number then
          puts"\n恐竜に1万ruby奪われてしまった。"
          sth = $dbh.execute("select user_name,money from userinfo where user_id = #{$data_number};")
          sth.each do |row|
            printf("\nユーザネーム: %s, 所持金: %s", row[0], row[1])
          end
          print " --------> "
          sth = $dbh.execute("update userinfo set money = money - 10000 where user_id = #{$data_number};")
          sth = $dbh.execute("select money from userinfo where user_id = #{$data_number};")
          sth.each do |row|
            printf("所持金: %s", row[0])
            puts ""
            puts ""
          end
        else
          puts " \n ．．．。特に何も起こらなかった。"
          puts ""
          puts ""
        end
        puts "\n(Enterを押してください。)"
        gets.chomp
        print "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n"

      when 2
        $throw_two_count += 1
        puts "\n恐竜と対決。対決に勝利し！「角」1本ゲットした。アイテムに追加した。"
        sth = $dbh.execute("select user_name,item from userinfo where user_id = #{$data_number};")
        sth.each do |row|
          printf("\nユーザネーム: %s, アイテム数: %s個", row[0], row[1])
        end
        print " --------> "
        sth = $dbh.execute("update userinfo set item = item + 1 where user_id = #{$data_number};")
        sth = $dbh.execute("select item from userinfo where user_id = #{$data_number};")
        sth.each do |row|
          printf("アイテム数: %s個", row[0])
          puts ""
          puts ""
        end
        $corner_flag += 1
        puts "\n(Enterを押してください。)"
        gets.chomp
        print "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n"

      when 3
        $throw_three_count += 1
        puts "\n恐竜の群れに遭遇。あえなく恐竜の群れにやられる。所持金を半分失った。"
        sth = $dbh.execute("select user_name,money from userinfo where user_id = #{$data_number};")
        sth.each do |row|
          printf("\nユーザネーム: %s, 所持金: %s", row[0], row[1])
          @money = row[1]
        end
        print " --------> "
        if @money >= 0
          sth = $dbh.execute("update userinfo set money = money / 2 where user_id = #{$data_number};")
        else @money < 0
        end
        sth = $dbh.execute("select money from userinfo where user_id = #{$data_number};")
        sth.each do |row|
          printf("所持金: %s", row[0])
          puts ""
          puts ""
        end
        puts "\n(Enterを押してください。)"
        gets.chomp
        print "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n"

      when 4
        $throw_four_count += 1
        puts "\n「#{$user_name}」は地底人の村を恐竜の群れから救い平和な村を取り戻す。村の宝をもらう。
アイテムに追加された。"
        sth = $dbh.execute("select user_name,item from userinfo where user_id = #{$data_number};")
        sth.each do |row|
          printf("\nユーザネーム: %s, アイテム数: %s個", row[0], row[1])
        end
        print " --------> "
        sth = $dbh.execute("update userinfo set item = item + 1 where user_id = #{$data_number};")
        sth = $dbh.execute("select item from userinfo where user_id = #{$data_number};")
        sth.each do |row|
          printf("アイテム数: %s個", row[0])
          puts ""
          puts ""
        end
        $treasure_flag += 1
        puts "\n(Enterを押してください。)"
        gets.chomp
        print "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n"

      when 5
        $throw_five_count += 1
        puts "\n突然光が差し込む。ピカーン。"
        puts "\n(Enterを押してください。)"
        gets.chomp
        print "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n"
        puts "\n光の正体は隕石だった。"
        puts "\n(Enterを押してください。)"
        gets.chomp
        print "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n"
        puts "\n隕石が財布を直撃する。所持金をすべて失った。"
        sth = $dbh.execute("select user_name,money from userinfo where user_id = #{$data_number};")
        sth.each do |row|
          printf("\nユーザネーム: %s, 所持金: %s", row[0], row[1])
          @money = row[1]
        end
        print " --------> "
        if @money >= 0
          sth = $dbh.execute("update userinfo set money = 0 where user_id = #{$data_number};")
        else @money < 0
        end
        sth = $dbh.execute("select money from userinfo where user_id = #{$data_number};")
        sth.each do |row|
          printf("所持金: %s", row[0])
          puts ""
          puts ""
        end
        puts "\n(Enterを押してください。)"
        gets.chomp
        print "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n"

      when 6
        $throw_six_count += 1
        puts ""
        puts ""
        break
      end
    end
  end
end

#event = Cretaceous.new
#event.branch

#require 'rdbi'
#require 'rdbi-driver-sqlite3'
#require 'color_echo'
#require './AAlist'

class Nara
  $japan_coin_flag = 0

  def branch
    nara_logo = AAlist.new
    nara_logo.nara
    puts "(Enterを押してください。)"
    gets.chomp

    #サイコロの目の回数を数えるための変数
    one_event = 0
    two_event = 0
    three_event = 0
    four_event = 0
    loop do
      puts "\nサイコロを振ります。(Y/yを押してください。)"
      yesno = gets.chomp.upcase
      print "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n"
      if /^Y$/ =~ yesno
        number = rand(6) + 1
        one_event += 1 if number == 1
        two_event += 1 if number == 2
        three_event += 1 if number == 3
        four_event += 1 if number == 4
        if one_event >= 3 || two_event >= 3 || three_event >= 3 || four_event >= 3 then
          number = 6
          # サイコロの目を表示
          dice = AAlist.new
          case number
          when 1
            dice.dice1
          when 2
            dice.dice2
          when 3
            dice.dice3
          when 4
            dice.dice4
          when 5
            dice.dice5
          when 6
            dice.dice6
          end
          puts "(Enterを押してください。)"
          gets.chomp

        else
          # サイコロの目を表示
          dice = AAlist.new
          case number
          when 1
            dice.dice1
          when 2
            dice.dice2
          when 3
            dice.dice3
          when 4
            dice.dice4
          when 5
            dice.dice5
          when 6
            dice.dice6
          end
          puts "(Enterを押してください。)"
          gets.chomp

        end
      end

      case number
      when 1
        $throw_one_count += 1
        puts "\n日本最古の寿司を食べる。"
        puts "\n(Enterを押してください。)"
        gets.chomp
        print "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n"
        puts "\n -豆知識-
稲作とともに中国から伝わりました。炊いた米と魚を一緒に漬け、米の発酵を
利用して魚を保存する技術が日本に入ってきたのが奈良時代だとされています。"
        puts "\n(Enterを押してください。)"
        gets.chomp
        print "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n"
        puts "\n1万rubyを使用する。"
        sth = $dbh.execute("select user_name,money from userinfo where user_id = #{$data_number};")
        sth.each do |row|
          printf("\nユーザネーム: %s, 所持金: %s", row[0], row[1])
        end
        print " --------> "
        sth = $dbh.execute("update userinfo set money = money - 10000 where user_id = #{$data_number};")
        sth = $dbh.execute("select money from userinfo where user_id = #{$data_number};")
        sth.each do |row|
          printf("所持金: %s", row[0])
          puts ""
          puts ""
        end
        puts "\n(Enterを押してください。)"
        gets.chomp
        print "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n"

      when 2
        $throw_two_count += 1
        puts "\n和同開珎銀銭を手に入れる。アイテムに追加された。"
        sth = $dbh.execute("select user_name,item from userinfo where user_id = #{$data_number};")
        sth.each do |row|
          printf("\nユーザネーム: %s, アイテム数: %s個", row[0], row[1])
        end
        print " --------> "
        sth = $dbh.execute("update userinfo set item = item +1  where user_id = #{$data_number};")
        sth = $dbh.execute("select item from userinfo where user_id = #{$data_number};")
        sth.each do |row|
          printf("アイテム数: %s個", row[0])
          puts ""
          puts ""
        end
        $japan_coin_flag += 1
        puts "\n(Enterを押してください。)"
        gets.chomp
        print "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n"

      when 3
        $throw_three_count += 1
        puts "\n13回に渡り、遣唐使として大活躍！500万ruby獲得。"
        sth = $dbh.execute("select user_name,money from userinfo where user_id = #{$data_number};")
        sth.each do |row|
          printf("\nユーザネーム: %s, 所持金: %s", row[0], row[1])
        end
        print " --------> "
        sth = $dbh.execute("update userinfo set money = money + 5000000 where user_id = #{$data_number};")
        sth = $dbh.execute("select money from userinfo where user_id = #{$data_number};")
        sth.each do |row|
          printf("所持金: %s", row[0])
          puts ""
          puts ""
        end
        puts "\n(Enterを押してください。)"
        gets.chomp
        print "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n"

      when 4
        $throw_four_count += 1
        puts "\n偶然にも万葉集制作現場に訪れた。"
        puts "\n(Enterを押してください。)"
        gets.chomp
        print "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n"
        puts "\n家来) 和歌が全然集まらんではないか。これでは仁徳天皇になんと申せば良いものか・・・。"
        puts "\n(Enterを押してください。)"
        gets.chomp
        print "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n"
        puts "\n家来）おぬしー。"
        puts "\n(Enterを押してください。)"
        gets.chomp
        puts "\n家来）おぬしー。"
        puts "\n(Enterを押してください。)"
        gets.chomp 
        print "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n"
        puts "\n家来）困っとるのじゃ。ぜひ一句頼む！！"
        puts "\n(Enterを押してください。)"
        gets.chomp
        print "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n"
        puts "\n万葉集を書き足すことになった。"
        puts "\n(五・七・五を考えてください。)"
        puts "\n五"
        f = gets.chomp
        puts "\n七"
        s = gets.chomp
        puts "\n五"
        t = gets.chomp

        puts "
      -------------------------------------
      |                                   |
      |                                   |
      |             #{t[0]}    #{s[0]}    #{f[0]}        |
      |             #{t[1]}    #{s[1]}    #{f[1]}        |
      |             #{t[2]}    #{s[2]}    #{f[2]}        |
      |             #{t[3]}    #{s[3]}    #{f[3]}        |
      |             #{t[4]}    #{s[4]}    #{f[4]}        |
      |                   #{s[5]}              |
      |                   #{s[6]}              |
      |                                   |
      |                                   |
      |                                   |
      -------------------------------------
"

        #File.open("manyoshu.txt","w") do |text|
        #  sentences = gets.chomp
        #  text.puts ("#{sentences}")
        #end
        puts ""
        puts "\n(Enterを押してください。)"
        gets.chomp
        print "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n"

      when 5
        $throw_five_count += 1
        puts ""
        puts ""
        require './ModernTime'
        present_day = ModernTime.new
        present_day.goal

      when 6
        $throw_six_count += 1
        puts ""
        puts ""
        puts ""

        break
      end

    end
  end
end

 #event = Nara.new
 #event.branch

require 'color_echo' #文字色、背景色を色付けする
#require 'rdbi'
#require 'rdbi-driver-sqlite3'
require './AAlist'

class TimeSlip
  #サイコロの出た目をカウントする変数
  $throw_one_count = 0
  $throw_two_count = 0
  $throw_three_count = 0
  $throw_four_count = 0
  $throw_five_count = 0
  $throw_six_count = 0

  def throw
    sep = "\n"
    #登場人物に声を掛けられタイムスリップ開始
    characters = ["おじさん", "少年", "アンパンマン", "ピーターパン", "藤〇健太"]
    puts "\n\nおや！？誰かが近寄ってきたぞ。(Enterを押してください。)\n"
    gets.chomp
    print "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n"
    print characters.sample
    print "　に声をかけられた。(Enterを押してください。)"
    gets.chomp
    print "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n"
    print "\nさぁ、君のサイコロを振ってくれ。(Enterを押してください。)"
    gets.chomp
    print "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n"

    # サイコロを振る
    puts "\n\n\n\nサイコロを振ります。(Y/yを押してください。)"
    yesno = gets.chomp.upcase
    print "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n"
    if /^Y$/ =~ yesno
      $number = rand(6) + 1
      # サイコロの目を表示
      dice = AAlist.new
      case $number
      when 1
        dice.dice1
      when 2
        dice.dice2
      when 3
        dice.dice3
      when 4
        dice.dice4
      when 5
        dice.dice5
      when 6
        dice.dice6
      end
      puts "(Enterを押してください。)"
      gets.chomp

    elsif /^S$/ =~yesno
      # 裏技S
      require './ModernTime'
      modern = ModernTime.new
      modern.goal
      gets.chomp
    else
      throw
    end

    toak_about = ["\n\nお前を夢の世界へ導こう！\nそりゃタイムスリップ！！", "\n\nタイムスリップするよ、僕についてきて！", "\n\n覚悟しろぉー！！\nタイムスリップじゃww"]
    print toak_about.sample
    print "\n(Enterを押してください。)"
    gets.chomp
    print "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n"

    # タイムスリップのエフェクトをアスキーアートで表現(省略予定)
    # print ""
    #

  end

  def epoch
    while true
      require './EndCondition'
      move = EndCondition.new
      move.finish
      throw
        case $number
        when 1
          $throw_one_count += 1
          # 現代
          require './Present'
          move = Present.new
          move.branch

        when 2
          $throw_two_count += 1
          # 白亜紀
          require './Cretaceous'
          move = Cretaceous.new
          move.branch

        when 3
          $throw_three_count += 1
          # 2150年　未来
          require './Future'
         move =  Future.new
         move.branch

        when 4
          $throw_four_count += 1
          # 1295年　未来
          require './Italy'
          move = Italy.new
          move.branch

        when 5
          $throw_five_count += 1
          # 710年　奈良
          require './Nara'
          move = Nara.new
          move.branch

        when 6
          $throw_six_count += 1
          # 1500年　フランス
          require './France'
          move = France.new
          move.branch

          #break
        end
    end
  end
end

# ここからがアプリケーションを動かす本体

#event = TimeSlip.new
# test = Database.new("../userinfos.db")
#event.epoch

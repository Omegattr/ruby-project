#require 'rdbi'
#require 'rdbi-driver-sqlite3'
#require 'color_echo'
#require './AAlist'

#現代
class Present
  $sword_flag = 1

  def branch
    present_logo = AAlist.new
    present_logo.present
    puts "(Enterを押してください。)"
    gets.chomp

    #サイコロの目の回数を数えるための変数
    one_event = 0
    two_event = 0
    three_event = 0
    four_event = 0
    five_event = 0
    loop do
      puts "\nサイコロを振ります。(Y/yを押してください。)"
      yesno = gets.chomp.upcase
        print "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n"
      if /^Y$/ =~ yesno
        number = rand(6) + 1
      
        one_event += 1 if number == 1
        two_event += 1 if number == 2
        three_event += 1 if number == 3
        four_event += 1 if number == 4
        five_event += 1 if number == 5
        
        #サイコロの同じ目の出る回数の上限を3に設定
        if one_event >= 3 || two_event >= 3 || three_event >= 3 || four_event >= 3 || five_event >= 3 then
          number = 6
          # サイコロの目を表示
          dice = AAlist.new
          case number
          when 1
            dice.dice1
          when 2
            dice.dice2
          when 3
            dice.dice3
          when 4
            dice.dice4
          when 5
            dice.dice5
          when 6
            dice.dice6
          end
          puts "(Enterを押してください。)"
          gets.chomp

        else
          # サイコロの目を表示
          dice = AAlist.new
          case number
          when 1
            dice.dice1
          when 2
            dice.dice2
          when 3
            dice.dice3
          when 4
            dice.dice4
          when 5
            dice.dice5
          when 6
            dice.dice6
          end
          puts "(Enterを押してください。)"
          gets.chomp

        end
      end

      case number
      when 1
        $throw_one_count += 1
        puts "\n街並みに驚き財布を落とす。
所持金の半分を失う。(マイナスの場合はそのまま)"
        sth = $dbh.execute("select user_name,money from userinfo where user_id = #{$data_number};")
        sth.each do |row|
          printf("\nユーザネーム: %s, 所持金: %s", row[0], row[1])
          @money = row[1]
        end
        print " --------> "
        if @money >= 0
          sth = $dbh.execute("update userinfo set money = money / 2 where user_id = #{$data_number};")
        else @money < 0
        end
        sth = $dbh.execute("select money from userinfo where user_id = #{$data_number};")
        sth.each do |row|
          printf("所持金: %s", row[0])
          puts ""
          puts ""
        end
        puts "\n(Enterを押してください。)"
        gets.chomp
        print "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n"

      when 2
        $throw_two_count += 1
        puts "\n自動販売機で100rubyのお茶を買う。のどが潤う。"
        sth = $dbh.execute("select user_name,money from userinfo where user_id = #{$data_number};")
        sth.each do |row|
          printf("\nユーザネーム: %s, 所持金: %s", row[0], row[1])
        end
        print " --------> "
        sth = $dbh.execute("update userinfo set money = money - 100 where user_id = #{$data_number};")
        sth = $dbh.execute("select money from userinfo where user_id = #{$data_number};")
        sth.each do |row|
          printf("所持金: %s", row[0])
          puts ""
          puts ""
        end
        puts "\n(Enterを押してください。)"
        gets.chomp
        print "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n"

      when 3
        $throw_three_count += 1 
        puts "\nOKで買い物。店員さんが勧めるがままに　にんじん、じゃがいも、玉ねぎ、牛肉　
を購入した。2,000ruby使う。"
        sth = $dbh.execute("select user_name,money from userinfo where user_id = #{$data_number};")
        sth.each do |row|
          printf("\nユーザネーム: %s, 所持金: %s", row[0], row[1])
        end
        print " --------> "
        sth = $dbh.execute("update userinfo set money = money - 2000 where user_id = #{$data_number};")
        sth = $dbh.execute("select money from userinfo where user_id = #{$data_number};")
        sth.each do |row|
          printf("所持金: %s", row[0])
          puts ""
          puts ""
        end
        puts "\n(Enterを押してください。)"
        gets.chomp
        print "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n"

      when 4
        $throw_four_count += 1
        puts "\n江戸時代から持ってきた刀を売る。メルーカリで1万rubyで売れる。"
        sth = $dbh.execute("select user_name,money from userinfo where user_id = #{$data_number};")
        sth.each do |row|
          printf("\nユーザネーム: %s, 所持金: %s", row[0], row[1])
        end
        print " --------> "
        sth = $dbh.execute("update userinfo set money = money + 10000 where user_id = #{$data_number};")
        sth = $dbh.execute("select money from userinfo where user_id = #{$data_number};")
        sth.each do |row|
          printf("所持金: %s", row[0])
          puts ""
          puts ""
        end
        $sword_lose_flag = 0
        puts "\n(Enterを押してください。)"
        gets.chomp
        print "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n"

      when 5
        $throw_five_count += 1
        puts "\n1,000ruby拾った。ラッキー♪"
        sth = $dbh.execute("select user_name,money from userinfo where user_id = #{$data_number};")
        sth.each do |row|
          printf("\nユーザネーム: %s, 所持金: %s", row[0], row[1])
        end
        print " --------> "
        sth = $dbh.execute("update userinfo set money = money + 1000 where user_id = #{$data_number};")
        sth = $dbh.execute("select money from userinfo where user_id = #{$data_number};")
        sth.each do |row|
          printf("所持金: %s", row[0])
          puts ""
          puts ""
        end
        puts "\n(Enterを押してください。)"
        gets.chomp
        print "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n"

      when 6
        $throw_six_count += 1	
        puts ""
        puts ""
        puts ""
        break
      end
    end
  end
end

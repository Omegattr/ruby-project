
require 'color_echo/get' #文字色、背景色を色付けする
require 'rdbi'
require 'rdbi-driver-sqlite3'
require './AAlist'
require './Timeslip'

require './Story'

class Edo
  # Edoクラスのインスタンス初期化
  def initialize( user_name, gender )

    $user_name = user_name
    @gender = gender
  end

  # アクセサ
  attr_accessor = :user_name, :gender

  # Edoクラスのインスタンスの文字列表現を返す
  def to_s

    "#{$user_name}, #{@gender}, #{@dice}"
  end

  # ユーザーネーム
  def to_formatted_string( sep = "\n" )

    "ユーザーネーム： #{$user_name}　　性別： #{@gender}"
  end
end

# EdoEntrysクラスを定義
class EdoEntry

  # 処理選択画面
  def run
    print "\n\n\n\n


                                         .                                      .,                                   ...
                                        -E   .         .M%      dF        dNa, JN,8             .,                   b J
                                       -B```TM!      .MD       .E        .. _          W]    .  .TMe     .    .J.  (N,
                                     .MD.g,.dF    .(MMF       .M^  .     TMb     .M^ ...MgT`4M^         .M!   JF    ?N,  .(((((((((((,
                                          ?MN,  TH`  dF       dF   TN,         .MD   7  -b .E         .MD    (E      ?M,
                                        ..M= `       dF    (gdNNNMMMMMp   ...ME`         d[     ...JMM`     d@        4E
                                      .MB^           dF              .^   T`!            .E     77!



"
    print "\n\n\n\n\n\n\n\n\n\n\n\n\n"
    # 時間があれば「はじめから」と「つづきから」にする
    print "
                                                                             1. はじめる
                                                                             9. おわる


\n\n(1, 9 を押してください。)"
    print "\n"

    #文字の入力を待つ
    num = gets.chomp
    # 項目を増やすならcase文で
    case num
    when '1'
      # ユーザーネームと性別を登録
      add_user_info
    when '9'
      # ゲームの終了
      exit
    else
      run
    end
  end

  def initialize( userinfos )
    # SQLiteデータベースファイルに接続
    $db_name = userinfos
    $dbh = RDBI.connect( :SQLite3, :database => $db_name )
  end

  # ユーザーネームと性別を登録
  def add_user_info

    print "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n   ユーザー登録\n\n\n\n\n\n\n\n\n\n\n"
    print "\n      ユーザーネームを登録します"
    # ユーザーデータ１件分のインスタンスを作成
    user_data = Edo.new( "", "" )
    print "\n\n"
    print "      ユーザーネーム :"

    $user_name = gets.chomp.encode("UTF-8")
    if $user_name.empty?
      CE.once.pickup(/ユーザーネームを入力してください/, :red)
      puts "      ユーザーネームを入力してください(Enterを押してください。)"
      puts ""
      gets.chomp
      add_user_info
    end
    print "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n   ユーザー登録\n\n\n\n\n\n\n\n\n"
    print "\n      性別を登録します"
    puts "\n\n      男性：M/m　女性：F/f"
    puts "      (M/m, F/f を押してください。)"
    
    mf = gets.chomp.upcase
    if /^M$/ =~ mf
      @gender = "男性"
    elsif /^F$/ =~ mf
      @gender = "女性"
    else
      print "      " + mf
      CE.once.pickup(/は不正です！最初から入力してください/, :red)
      print "　は不正です！最初から入力してください(Enterを押してください。)"
      gets.chomp
      add_user_info
    end

    # 登録内容を確認する

    print "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n   ユーザー登録\n\n\n\n\n\n\n"
    print "\n      ユーザーネーム：  "

    print $user_name

    print "  　性別：  "
    print @gender

    print "\n      で登録してよろしいですか？"
    print "\n\n\n      はい：Y/y　いいえ：N/n　\n"
    puts "      (Y/y, N/n を押してください。)"
    yn = gets.chomp.upcase
    while yn do
      if /^Y$/ =~ yn
        # Yなら登録
        # データベース確認

        # もしデータ１があればデータ２を作成。１も２もあれば選んで上書き
        $user_info_data = $dbh.execute("select user_id, user_name, money, item from userinfo;").fetch(:all)
        if $user_info_data.size >= 2
          # 1も2もあった時選んで削除
          puts "\n\n\n\n\n\n\n"
          puts "       セーブデータがいっぱいです。"

          puts "       No1 か No2 を選んで削除してください。"
          puts "\n"
          $user_info_data.each do |user|
            puts "   No             :   #{user[0]}"
            puts "   ユーザーネーム :   #{user[1]}"
            puts "   所持金         :   #{user[2]}"
            puts "   アイテム       :   #{user[3]} 個"
            puts "   ------------------"
          end
          print "\n\n\n"
          puts "(1, 2 を押してください。)"
          sel = gets.chomp.upcase
          if /1/ =~ sel
            $dbh.execute("update userinfo set user_name = '#{$user_name}', gender = '#{@gender}', money = -1000000, item = 1 where user_id = 1;")
            $dbh.execute("update statistics set user_name = '#{$user_name}', mode = 0, sum = 0, number_of_times = 0 where user_id = 1;")
            puts "       登録しました"
            $data_number = 1
            # ストーリーを進める
            edo_story
          elsif /2/ =~ sel


            $dbh.execute("update userinfo set user_name = '#{$user_name}', gender = '#{@gender}', money = -1000000, item = 1 where user_id = 2;")
            $dbh.execute("update statistics set user_name = '#{$user_name}', mode = 0, sum = 0, number_of_times = 0 where user_id = 2;")
            puts "      登録しました"
            $data_number = 2
            # ストーリーを進める
            edo_story
          end
        end
      elsif /^N$/ =~ yn
        puts "\n       もう一度入力してください"
        # Nならもう一度ユーザーネームから入力
        add_user_info
      end
      CE.once.pickup(/不正な値が入力されています/, :red)
      puts "       不正な値が入力されています(Y/y, N/n を押してください。)" 
      yn = gets.chomp.upcase
    end
  end


  def edo_story

    # イベント名表示
    edo_logo = AAlist.new

    edo_logo.edo

    puts "(Enterを押してください。)"
    str = Story.new
    # あらすじ
    puts str.scenario_a
    print "\n"


    # タイムスリップに繋げる
    event = TimeSlip.new
    event.epoch

  end
end

# ここからがアプリケーションを動かす本体

# アプリケーションのインスタンスを作る
# ユーザーデータのSQLite3のデータベースを指定している
edo_entry = EdoEntry.new("../userinfos.db")

# EdoEntryの処理の選択と選択後の処理を繰り返す
edo_entry.run

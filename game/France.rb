#require 'rdbi'
#require 'rdbi-driver-sqlite3'
#require 'color_echo'
#require './AAlist'

class France
  def branch
    france_logo = AAlist.new
    france_logo.france
    puts "(Entryを押してください。)"
    gets.chomp

    #サイコロの目の回数を数えるための変数
    one_event = 0
    two_event = 0
    three_event = 0
    four_event = 0
    five_event = 0
    loop do
      puts "\nサイコロを振ります。(Y/yを押してください。)"
      yesno = gets.chomp.upcase
      print "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n"
      if /^Y$/ =~ yesno
        number = rand(6) + 1
        one_event += 1 if number == 1
        two_event += 1 if number == 2
        three_event += 1 if number == 3
        four_event += 1 if number == 4
        five_event += 1 if number == 5
        if one_event >= 3 || two_event >= 3 || three_event >= 3 || four_event >= 3 || five_event >= 3 then
          number = 6
          # サイコロの目を表示
          dice = AAlist.new
          case number
          when 1
            dice.dice1
          when 2
            dice.dice2
          when 3
            dice.dice3
          when 4
            dice.dice4
          when 5
            dice.dice5
          when 6
            dice.dice6
          end
          puts "(Enterを押してください。)"
          gets.chomp

        else
          # サイコロの目を表示
          dice = AAlist.new
          case number
          when 1
            dice.dice1
          when 2
            dice.dice2
          when 3
            dice.dice3
          when 4
            dice.dice4
          when 5
            dice.dice5
          when 6
            dice.dice6
          end
          puts "(Enterを押してください。)"
          gets.chomp

        end
      end

      case number
      when 1
        $throw_one_count += 1
        puts "\nヘンリー8世）タイムジャンパーの「#{$user_name}」さん。
わたくしとビーフステーキをご一緒しませんか？"
        puts "\n(Enterを押してください。)"
        gets.chomp
        print "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n"
        puts "\nヘンリー8世とビーフステーキを食べた。"
        puts "\n(Enterを押してください。)"
        gets.chomp
        print "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n"
        puts "\n-豆知識-
ヘンリー8世の食事にロイン（腰の部分）を出したところ、それを食べたヘンリー8世が
あまりのうまさにsir(サー)の称号を与えて、サーロインと呼ばれるようになったと言う
伝説がある。また、ビフテキとはフランス語が語源です。"
        puts "\n(Enterを押してください。)"
        gets.chomp
        print "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n"
        puts "\n3万ruby使用した。"
        sth = $dbh.execute("select user_name,money from userinfo where user_id = #{$data_number};")
        sth.each do |row|
          printf("\nユーザネーム: %s, 所持金: %s", row[0], row[1])
        end
        print " --------> "
        sth = $dbh.execute("update userinfo set money = money - 30000 where user_id = #{$data_number};")
        sth = $dbh.execute("select money from userinfo where user_id = #{$data_number};")
        sth.each do |row|
          printf("所持金: %s", row[0])
          puts ""
          puts ""
        end
        puts "\n(Enterを押してください。)"
        gets.chomp
        print "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n"

      when 2
        $throw_two_count += 1
        puts "\n「#{$user_name}」は騎士を目指しはじめる。勢いで馬を10頭購入する。500万ruby使用した。"
        sth = $dbh.execute("select user_name,money from userinfo where user_id = #{$data_number};")
        sth.each do |row|
          printf("\nユーザネーム: %s, 所持金: %s", row[0], row[1])
        end
        print " --------> "
        sth = $dbh.execute("update userinfo set money = money - 5000000  where user_id = #{$data_number};")
        sth = $dbh.execute("select money from userinfo where user_id = #{$data_number};")
        sth.each do |row|
          printf("所持金: %s", row[0])
          puts ""
          puts ""
        end
        puts "\n(Enterを押してください。)"
        gets.chomp
        print "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n"

      when 3
        $throw_three_count += 1
        puts "\n戦いに参戦！！見事勝利する。700万ruby獲得。"
        sth = $dbh.execute("select user_name,money from userinfo where user_id = #{$data_number};")
        sth.each do |row|
          printf("\nユーザネーム: %s, 所持金: %s", row[0], row[1])
        end
        print " --------> "
        sth = $dbh.execute("update userinfo set money = money + 7000000 where user_id = #{$data_number};")
        sth = $dbh.execute("select money from userinfo where user_id = #{$data_number};")
        sth.each do |row|
          printf("所持金: %s", row[0])
          puts ""
          puts ""
        end
        puts "\n(Enterを押してください。)"
        gets.chomp
        print "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n"

      when 4
        $throw_four_count += 1
        puts "\nブルゴーニュで高級ワインを購入する。「#{$user_name}」は歴史にひたっていた。4万ruby使用した。"
        sth = $dbh.execute("select user_name,money from userinfo where user_id = #{$data_number};")
        sth.each do |row|
          printf("\nユーザネーム: %s, 所持金: %s", row[0], row[1])
        end
        print " --------> "
        sth = $dbh.execute("update userinfo set money = money - 40000 where user_id = #{$data_number};")
        sth = $dbh.execute("select money from userinfo where user_id = #{$data_number};")
        sth.each do |row|
          printf("所持金: %s", row[0])
          puts ""
          puts ""
        end
        puts "\n(Enterを押してください。)"
        gets.chomp
        print "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n"

      when 5
        $throw_five_count += 1
        puts "\n海賊の就職面接で合格。カリブ海で大暴れする。"
        puts "\n(Enterを押してください。)"
        gets.chomp
        print "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n"
        puts "\n前方から海賊船「ブラックパール号」が現れる！"
        puts "\n(Enterを押してください。)"
        gets.chomp
        print "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n"
        puts "\nサイコロを振って偶数が出れば金品強奪、奇数が出れば金銭を奪われる。"
        puts "\n(Enterを押してください。)"
        gets.chomp
        print "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n"
        puts "\nFIGHT!!"
        puts "\n(Enterを押してください。)"
        gets.chomp
        print "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n"
        puts "\nサイコロを振ります。(Y/yを押してください。)"
        yesno = gets.chomp.upcase
        print "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n"
        if /^Y$/ =~ yesno
          number = rand(6) + 1
          dice = AAlist.new
          case number
          when 1
            dice.dice1
          when 2
            dice.dice2
          when 3
            dice.dice3
          when 4
            dice.dice4
          when 5
            dice.dice5
          when 6
            dice.dice6
          end
          puts "(Enterを押してください。)"
          if number == 2 || number == 4 || number == 6 then
            puts "\n海賊に勝利し、金品強奪に成功！！700万ruby獲得。"
            sth = $dbh.execute("select user_name,money from userinfo where user_id = #{$data_number};")
            sth.each do |row|
              printf("\nユーザネーム: %s, 所持金: %s", row[0], row[1])
            end
            print " --------> "
            sth = $dbh.execute("update userinfo set money = money + 7000000 where user_id = #{$data_number};")
            sth = $dbh.execute("select money from userinfo where user_id = #{$data_number};")
            sth.each do |row|
              printf("所持金: %s", row[0])
              puts ""
              puts ""
            end
          else
            puts "\n海賊に負け、金銭を奪われました...。80万ruby奪われた。"
            sth = $dbh.execute("select user_name,money from userinfo where user_id = #{$data_number};")
            sth.each do |row|
              printf("\nユーザネーム: %s, 所持金: %s", row[0], row[1])
            end
            print " --------> "
            sth = $dbh.execute("update userinfo set money = money - 800000 where user_id = #{$data_number};")
            sth = $dbh.execute("select money from userinfo where user_id = #{$data_number};")
            sth.each do |row|
              printf("所持金: %s", row[0])
              puts ""
              puts ""
            end
          end
        end

        puts "\n(Enterを押してください。)"
        gets.chomp
        print "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n"

      when 6
        $throw_six_count += 1
        puts ""
        puts ""
        puts ""
        break
      end
    end
  end
end


#event = France.new
#event.branch

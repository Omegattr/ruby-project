DROP TABLE IF EXISTS statistics;
CREATE TABLE statistics (
    user_id                   varchar(1) NOT NULL,
    user_name                 varchar(20) NOT NULL,
    mode                   integer(1) NOT NULL,
    sum                       integer NOT NULL,
    number_of_times           integer NOT NULL,
    CONSTRAINT pk_statistics PRIMARY KEY (user_id),
    FOREIGN KEY(user_name) REFERENCES userinfo(user_name)
);
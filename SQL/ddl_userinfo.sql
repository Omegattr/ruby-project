DROP TABLE IF EXISTS userinfo;
CREATE TABLE userinfo (
    user_id                   varchar(1) NOT NULL,
    user_name                 varchar(20) NOT NULL,
    gender                    varchar(4) NOT NULL,
    money                     integer DEFAULT(-1000000) NOT NULL,
    item                      integer,
    total_money               integer NOT NULL,
    CONSTRAINT pk_userinfo PRIMARY KEY (user_id)
);
DROP TABLE IF EXISTS household_account_books;
CREATE TABLE household_account_books (
    id                        integer NOT NULL,
    product_name              varchar(200) NOT NULL,
    sum                       integer NOT NULL,
    date                      date,
    CONSTRAINT pk_household_account_books PRIMARY KEY (id)
);